package com.alias.test.common;

public final class AppProperties {

	private AppProperties() {}
	
	public static final String APP_NAME = "TEST";
	public static final String REL_VERSION = "1.0.0";
	public static final String REL_DATE = "10/09/2017";
}
