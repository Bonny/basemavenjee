package com.alias.test.web.service;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.alias.test.web.bean.RestResponse;
import com.alias.test.web.exception.RestException;

abstract class AService {

	@Context
	HttpServletRequest servletRequest;

	Logger getLog() {
		return Logger.getLogger(getClass());
	}

	String getLogPrefix() {
		return (servletRequest != null) ? String.format("[sessUUID=%s] ", servletRequest.getSession().getId())
				: "NO-LOGPREFIX";
	}

	Response ok() {
		return ok(null);
	}

	Response ok(Collection<?> data) {
		RestResponse rr = new RestResponse(data);
		return Response.status(rr.getHttpStatus()).entity(rr).build();
	}

	Response ok(Object data) {
		RestResponse rr = new RestResponse(data);
		return Response.status(rr.getHttpStatus()).entity(rr).build();
	}

	Response error(Error e) {
		return exception(new RestException());
	}

	Response exception(String message, Exception ex) {

		getLog().error(message, ex);

		RestResponse rr = null;

		if (ex instanceof RestException) {

			rr = new RestResponse(((RestException) ex).getError());

		} else {

			rr = new RestResponse(new RestException().getError());

		}

		getLog().debug(getLogPrefix() + rr);

		return Response.status(rr.getHttpStatus()).entity(rr).build();
	}

	Response exception(Exception ex) {
		return exception("Fatal error", ex);
	}
}
