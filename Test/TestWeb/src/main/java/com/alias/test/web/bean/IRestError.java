package com.alias.test.web.bean;

import javax.ws.rs.core.Response;

public interface IRestError {

	public String getCode();

	public String getDescription();
	
	default public int getHttpStatus() {
		return Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
	}
}
