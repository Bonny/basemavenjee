package com.alias.test.web.service;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.test.da.session.UserDA;

@Path("/user")
@RequestScoped
public class UserService extends AService {

	@EJB
	private UserDA userDA;

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response list() {
		try {
			return ok(userDA.getUsers());
		} catch (Exception ex) {
			return exception(ex);
		}
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response add(@FormParam("userid") String userid, @FormParam("age") @DefaultValue("0") int age) {
		try {
			getLog().info("userid=" + userid);
			getLog().info("age=" + age);
			userDA.addUser(userid, age);
			return ok();
		} catch (Exception ex) {
			return exception(ex);
		}
	}

}
