package com.alias.test.web.enums;

import com.alias.test.web.bean.IRestError;

public enum ErrorCode implements IRestError {

	/**
	 * {@value}
	 */
	OK("OK", "OK"),
	/**
	 * {@value}
	 */
	UNKNOW("UNKNOW", "Unknow error")

	;

	private String code, description;

	private ErrorCode(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getDescription() {
		return description;
	}

}
