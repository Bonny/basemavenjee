package com.alias.test.web.exception;

import com.alias.test.web.bean.IRestError;
import com.alias.test.web.enums.ErrorCode;

public class RestException extends Exception {

	private static final long serialVersionUID = 1L;

	IRestError error = ErrorCode.UNKNOW;

	public IRestError getError() {
		return error;
	}

	public RestException() {
		error = ErrorCode.UNKNOW;
	}

	public RestException(String message, IRestError error) {
		super(message);
		error = ErrorCode.UNKNOW;
	}

	public RestException(IRestError error, Throwable cause) {
		super(cause);
		this.error = error;
	}

	public RestException(String message, Throwable cause) {
		super(message, cause);
		error = ErrorCode.UNKNOW;
	}

	public RestException(String message, IRestError error, Throwable cause) {
		super(message, cause);
		this.error = error;
	}

	@Override
	public String getMessage() {

		final StringBuilder s = new StringBuilder(super.getMessage());

		if (getError() != null)
			s.append("[").append(getError().getCode()).append(",").append(getError().getDescription()).append("]");

		return s.toString();
	}

}
