package com.alias.test.web.service;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.test.ejb.TestSession;

@Path("/test")
@RequestScoped
public class TestService extends AService {

	@GET
	@Path("/hello/{s}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response hello(@PathParam("s") String s) {
		try {
			return ok(s);
		} catch (Exception ex) {
			return exception(ex);
		}
	}

	@EJB
	private TestSession ts;

	@GET
	@Path("/ejb/{s}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response ejb(@PathParam("s") String s) {
		try {
			return ok(ts.hello(s));
		} catch (Exception ex) {
			return exception(ex);
		}
	}
}
