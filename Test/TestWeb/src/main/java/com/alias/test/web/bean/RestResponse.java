package com.alias.test.web.bean;

import java.io.Serializable;

import javax.ws.rs.core.Response;

public class RestResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private Object data;
	private String errorCode;
	private String errorDesc;
	private int httpStatus;

	public RestResponse() {
		this.httpStatus = Response.Status.OK.getStatusCode();
	}

	public RestResponse(Object data) {
		this();
		this.data = data;
	}

	public RestResponse(IRestError err) {
		this.errorCode = err.getCode();
		this.errorDesc = err.getDescription();
		this.httpStatus = err.getHttpStatus();
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	@Override
	public String toString() {
		return "BaseResponse [data=" + data + ", errorCode=" + errorCode + ", errorDesc=" + errorDesc + ", httpStatus="
				+ httpStatus + "]";
	}
}
