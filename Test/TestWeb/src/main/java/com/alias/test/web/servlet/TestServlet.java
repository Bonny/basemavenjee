package com.alias.test.web.servlet;

import java.io.IOException;
import java.util.Date;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.test.ejb.TestSession;

@WebServlet("/test")
public final class TestServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@EJB
	private TestSession ts;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append(ts.hello("test: " + new Date()));
	}

}
