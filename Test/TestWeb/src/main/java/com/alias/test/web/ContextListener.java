package com.alias.test.web;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import com.alias.test.common.AppProperties;

@WebListener
public final class ContextListener implements ServletContextListener {

	static final Logger LOGGER = Logger.getLogger(ContextListener.class);

	static final String DISPLAY = String.format("%s v%s - %s", AppProperties.APP_NAME, AppProperties.REL_VERSION,
			AppProperties.REL_DATE);

	public void contextDestroyed(ServletContextEvent arg0) {
		LOGGER.info(String.format("Destroy: %s", DISPLAY));
	}

	public void contextInitialized(ServletContextEvent arg0) {
		LOGGER.info(String.format("Init: %s", DISPLAY));
	}

}
