CREATE TABLE public.users
(
  userid character varying(32) NOT NULL,
  age integer DEFAULT 0,
  CONSTRAINT users_pkey PRIMARY KEY (userid)
)