package com.test.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

@Stateless
@LocalBean
public class TestSession {

	static final Logger LOGGER = Logger.getLogger(TestSession.class);

	public String hello(final String s) {
		LOGGER.info("invoke hello method");
		return s;
	}

}
