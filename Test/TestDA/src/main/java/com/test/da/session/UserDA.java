package com.test.da.session;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.test.da.entity.User;

/**
 * Session Bean implementation class UserDA
 */
@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class UserDA {

	@PersistenceContext
	private EntityManager em;

	public List<User> getUsers() {
		return em.createQuery("select u from User u order by u.userid").getResultList();
	}

	public void addUser(String userid, int age) {

		if (userid == null)
			throw new IllegalArgumentException("userid is null");

		User u = new User();
		u.setAge(age);
		u.setUserid(userid);

		em.persist(u);
		
	}

}
