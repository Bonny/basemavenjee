#!/bin/bash

SRC=/home/lbonaldo/src/MAVEN/Test
JBOSS=/home/lbonaldo/dev/wildfly-9.0.2.Final/bin
APP_VER=1.0.0
APP_NAME=TestEar-$APP_VER.ear
DIST=$SRC/TestEar/target/$APP_NAME

echo "-------------- START MAVEN ---------------"
mvn clean install
echo "-------------- END MAVEN ---------------"

echo "-------------- START DEPLOY ---------------"
$JBOSS/jboss-cli.sh  --connect --user=admin --password=elaidemo1 --commands="undeploy $APP_NAME"
$JBOSS/jboss-cli.sh  --connect --user=admin --password=elaidemo1 --commands="deploy $DIST --force --name=$APP_NAME --runtime-name=$APP_NAME"
echo "-------------- END DEPLOY ---------------"
